﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingCutscene : MonoBehaviour
{
    [SerializeField] Animator CutAnimator;
    [SerializeField] Animation SceneAnimation;

    [SerializeField] AudioSource VoiceLinesM;
    [SerializeField] AudioSource VoiceLineF;

    [SerializeField] GameObject WhiteScreen;

    void Start()
    {
        WhiteScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CutScene()
    { 
    
        

    }
}
