﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour
{
    public GameObject death_panel;
    public FirstPersonController FPS;

    public void DeathPanel()
    {
        death_panel.SetActive(true);
        FPS.m_WalkSpeed = 0;
      //  FPS.m_RunSpeed = 0;
        FPS.m_JumpSpeed = 0;

    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
