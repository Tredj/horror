﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Seeyou : MonoBehaviour
{
	public int rays;
	public int distance = 15;
	public float angle = 60;
	private Vector3 offset;
	private List<Enemy> enemyRay = new List<Enemy>();
	//public GameObject target;

	bool GetRaycast(Vector3 dir)
	{
		bool result = false;
		RaycastHit hit = new RaycastHit();
		Vector3 pos = transform.position + offset;
		if (Physics.Raycast(pos, dir, out hit, distance))
		{
			if (hit.transform.GetComponent<Enemy>() != null)
			{
                if (!enemyRay.Contains(hit.transform.GetComponent<Enemy>()))
				{ 
					enemyRay.Add(hit.transform.GetComponent<Enemy>());
				}

				result = true;
				Debug.DrawLine(pos, hit.point, Color.green);
			}
			else
			{
				Debug.DrawLine(pos, hit.point, Color.blue);
			}
		}
		else
		{
			Debug.DrawRay(pos, dir * distance, Color.red);
		}
		return result;
	}

	bool RayToScan()
	{
		List<Enemy> enemy = new List<Enemy>(enemyRay);
		enemyRay.Clear();
		bool result = false;
		bool a = false;
		bool b = false;
		float j = 0;
		for (int i = 0; i < rays; i++)
		{
			var x = Mathf.Sin(j);
			var y = Mathf.Cos(j);

			j += angle * Mathf.Deg2Rad / rays;

			Vector3 dir = transform.TransformDirection(new Vector3(x, 0, y));
			if (GetRaycast(dir)) a = true;

			if (x != 0)
			{
				dir = transform.TransformDirection(new Vector3(-x, 0, y));
				if (GetRaycast(dir)) b = true;
			}
		}

		if (a || b) result = true;
        foreach (Enemy item in enemy.Where(e => !enemyRay.Contains(e)))
        {
			item.See = false;
        }

		foreach (Enemy item in enemyRay)
		{
			item.See = true;
		}
		return result;
	}

	void Update()
	{
		RayToScan();
		//if (Vector3.Distance(transform.position, target.transform.position) < distance)
		//{
		//	if (RayToScan())
		//	{
		//		Debug.Log("+");
		//		target.GetComponent<Enemy>().See = true;
				
				
		//	}
		//	else
		//	{
		//		Debug.Log("-");
		//		target.GetComponent<Enemy>().See = false;
		//	}
		//}
		//else
		//{
			
		//}
	}
}
