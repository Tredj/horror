﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItemPickUp : MonoBehaviour
{
    public TextScript TS;

    void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            TS.characterStatus++;
            gameObject.SetActive(false);
        }
    }
}