﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.AI;

public class VignetteController : MonoBehaviour
{
    public VignetteAndChromaticAberration vignette;
    public AnimationCurve curve;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float maxDist = float.MaxValue;
        foreach (Enemy item in FindObjectsOfType<Enemy>())
        {
             NavMeshPath NavMeshPath = new NavMeshPath();
            

            if (NavMesh.CalculatePath(transform.position,item.transform.position,NavMesh.AllAreas,NavMeshPath))
            {
                float Dist = 0;
                Vector3 lastPoint = transform.position;
                foreach (Vector3 items in NavMeshPath.corners)
                {
                    Dist += Vector3.Distance(lastPoint, items);

                }
                if(maxDist > Dist)
                {
                    maxDist = Dist;
                }
            }
        }
        vignette.intensity = curve.Evaluate(maxDist);
    }
}
