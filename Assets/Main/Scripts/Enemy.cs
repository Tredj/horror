﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] Transform[] pos;
    [SerializeField] private float speed = 3.5f;
    private float accuracy = 0.5f;
    int currPoition;

    public int rays;
    public int distance = 15;
    public float angle = 60;
    private Vector3 offset;
    public Transform target;

    public bool See;
    public NavMeshAgent main;

    public float distanceDamage = 0.5f;

    private void Start()
    {
        main = GetComponent<NavMeshAgent>();
        //main.speed = speed;
    }
    private void LateUpdate()
    {


        if (currPoition != pos.Length)
        {
            Vector3 lookAtTarget = new Vector3(pos[currPoition].position.x,
                                               this.transform.position.y,
                                               pos[currPoition].position.z);

            //this.transform.LookAt(lookAtTarget);

            if (See == true)
            {
                main.SetDestination(target.position);
                main.speed = speed+1;
                
                if(Vector3.Distance(this.transform.position,target.position) < distanceDamage)
                {
                    target.GetComponent<PlayerController>().DeathPanel();
                }
                //this.transform.LookAt(target.position);
                //transform.Translate(0, 0, speed * Time.deltaTime);
            }
            else
            {
                main.SetDestination(lookAtTarget);
                main.speed = speed;
                //this.transform.LookAt(lookAtTarget);
                if (Vector3.Distance(this.transform.position, lookAtTarget) > accuracy)
                {

                    transform.Translate(0, 0, speed / 2 * Time.deltaTime);
                }
                else
                {
                    currPoition++;
                }
            }

        }
        else
        {
            currPoition = 0;
        }
    }
    private void Update()
    {
        if (Vector3.Distance(transform.position, target.position) < distance)
        {
            if (RayToScan())
                See = true;
        }
        else
        {
            See = false;
        }
    }

    bool GetRaycast(Vector3 dir)
    {
        bool result = false;
        RaycastHit hit = new RaycastHit();
        Vector3 pos = transform.position + offset;
        if (Physics.Raycast(pos, dir, out hit, distance))
        {
            if (hit.transform == target)
            {
                result = true;
                Debug.DrawLine(pos, hit.point, Color.green);
            }
            else
            {
                Debug.DrawLine(pos, hit.point, Color.blue);
            }
        }
        else
        {
            Debug.DrawRay(pos, dir * distance, Color.red);
        }
        return result;
    }

    bool RayToScan()
    {
        bool result = false;
        bool a = false;
        bool b = false;
        float j = 0;
        for (int i = 0; i < rays; i++)
        {
            var x = Mathf.Sin(j);
            var y = Mathf.Cos(j);

            j += angle * Mathf.Deg2Rad / rays;

            Vector3 dir = transform.TransformDirection(new Vector3(x, 0, y));
            if (GetRaycast(dir)) a = true;

            if (x != 0)
            {
                dir = transform.TransformDirection(new Vector3(-x, 0, y));
                if (GetRaycast(dir)) b = true;
            }
        }

        if (a || b) result = true;
        return result;
    }
}
