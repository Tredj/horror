﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vibrator : MonoBehaviour
{
    [Range(0f, 100f)]
    [SerializeField] private int _random;
    [SerializeField] private float _перезарядка;
    public GameObject лампочка;


    private void Start()
    {
        Horor();
    }

    public void Horor()
    {
        float _r = Random.Range(0f, 100f);

        if (_r >= _random)
        {
            лампочка.SetActive(true);
        }
        else if (_r <= _random)
        {
            лампочка.SetActive(false);
        }
        Invoke("Horor", _перезарядка);
    }
}
