﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screamer : MonoBehaviour
{
    public AudioSource scarySound;
    public Animation scaryAnimation;

    void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            scarySound.Play();
            scaryAnimation.Play();
        }
    }
}