﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScript : MonoBehaviour
{
    [SerializeField] AudioListener lisn;
    [SerializeField] AudioClip crowbarVoiceHintClip;
    [SerializeField] AudioClip keyVoiceHintClip;
    [SerializeField] AudioClip backgroundVoiceClip;
    [SerializeField] AudioSource SecterVoiceSource;
    [SerializeField] AudioSource crowbarHintSource;
    [SerializeField] AudioSource keyHintSource;
    [SerializeField]
    [Range(0,1)]
    float volumeMutator1;
    [SerializeField]
    [Range(0, 1)]
    float volumeMutator2;

    [SerializeField] GameObject q_Planks;
    [SerializeField] GameObject q_Key;
    [SerializeField] GameObject doors;

    [SerializeField] GameObject lightSource;

    [SerializeField] GameObject FuckingScaryDeath;

    public int characterStatus;

    void Start()
    {
        SecterVoiceSource.PlayOneShot(backgroundVoiceClip);
        characterStatus = 0;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            switch (characterStatus)
            {
                case 0:
                    crowbarHintSource.PlayOneShot(crowbarVoiceHintClip);
                    break;
                case 1:
                    q_Planks.SetActive(false);
                    q_Key.SetActive(true);
                    keyHintSource.PlayOneShot(keyVoiceHintClip);
                    crowbarHintSource.volume = volumeMutator1;
                    break;
                case 2:
                    doors.SetActive(false);
                    lightSource.SetActive(false);
                    crowbarHintSource.volume = volumeMutator2;
                    FuckingScaryDeath.SetActive(true);
                    break;
            }
        }
    }
}