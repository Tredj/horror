﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTrigger : MonoBehaviour
{
    [SerializeField] Animator handsAnimation1;
    [SerializeField] Animator handsAnimation2;
    [SerializeField] Animator handsAnimation3;
    [SerializeField] Animator doorAnimation;
    [SerializeField] GameObject EnemyLight;


    void Start()
    {
        EnemyLight.SetActive(false);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            doorAnimation.SetBool("open_door", true);
            handsAnimation1.SetBool("hands_up", true);
            handsAnimation2.SetBool("hands_up", true);
            handsAnimation3.SetBool("hands_up", true);
            EnemyLight.SetActive(true);
        }
    }
}